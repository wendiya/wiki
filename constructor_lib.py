from flask_login import UserMixin
from werkzeug.security import check_password_hash
from werkzeug.security import generate_password_hash


def create_user_base(db):
    class Users(db.Model, UserMixin):
        id = db.Column(db.Integer, primary_key=True)
        username = db.Column(db.String(20), nullable=False, unique=True)
        email = db.Column(db.String(120), nullable=False, unique=True)
        password_hash = db.Column(db.String(128))
        posts = db.relationship('Posts', backref='post')

        @property
        def password(self):
            raise AttributeError('password is not a readable attribute!')

        @password.setter
        def password(self, password):
            self.password_hash = generate_password_hash(password)

        def verify_password(self, password):
            return check_password_hash(self.password_hash, password)

        def __repr__(self):
            return '<Name %r>' % self.name

    return Users


def create_user_post(db):
    class Posts(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        title = db.Column(db.String(255))
        content = db.Column(db.Text)
        slug = db.Column(db.String(255))
        user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    return Posts
