import os

from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_ckeditor import CKEditor

from application_constructor import ApplicationConstructor
from constructor_lib import create_user_base
from constructor_lib import create_user_post


def main():
    # TODO: Сделать конфигурационный файл для входных данных.
    path_to_database = 'database.db'
    SECRET_KEY = os.urandom(32)

    app_constructor = ApplicationConstructor()
    app_constructor.define_configurations(path_to_database=path_to_database,
                                          secret_key=SECRET_KEY)

    login_manager = LoginManager()
    login_manager.init_app(app_constructor.app)
    login_manager.login_view = 'login'

    ckeditor = CKEditor()
    ckeditor.init_app(app_constructor.app)

    db = SQLAlchemy(app_constructor.app)

    user_post = create_user_post(db=db)
    user_model = create_user_base(db=db)

    db.create_all()

    app_constructor.define_login_managers(user_model=user_model,
                                          login_manager=login_manager)
    app_constructor.define_login_required_routers(database=db,
                                                  user_model=user_model,
                                                  user_post=user_post)
    app_constructor.define_routers(database=db,
                                   user_model=user_model,
                                   user_post=user_post)
    app_constructor.define_error_handlers()

    app_constructor.app.run()


if __name__ == "__main__":
    main()
