# Html pages names

PAGE_404 = "404.html"
PAGE_500 = "500.html"
UPDATE_PAGE = "update.html"
POST_EDIT_PAGE = 'edit_post.html'
POSTS_PAGE = "posts.html"
POST_PAGE = "post.html"
INDEX_PAGE = "index.html"
DASHBOARD_PAGE = "dashboard.html"
ADD_USER_PAGE = "add_user.html"
ADD_POST_PAGE = "add_post.html"
LOGIN_PAGE = 'login.html'

# API paths
# TODO: Поправить пути согласно REST API.

DELETE_USER_PATH = '/delete/<int:identifier>'
UPDATE_USER_PATH = '/update/<int:identifier>'
ADD_USER_PATH = '/user/add'

DELETE_POST_PATH = '/posts/delete/<int:identifier>'
EDIT_POST_PATH = '/posts/edit/<int:identifier>'
ADD_POST_PATH = '/add-post'

POSTS_PATH = '/posts'
POST_PATH = '/posts/<int:identifier>'
DASHBOARD_PATH = '/dashboard'
MAIN_PATH = '/'

LOGIN_PATH = '/login'
LOGOUT_PATH = '/logout'

