from flask import Flask
from flask import render_template
from flask import flash
from flask import redirect
from flask import url_for
from flask import request
from flask_login import login_user
from flask_login import current_user
from flask_login import login_required
from flask_login import logout_user
from loguru import logger
from werkzeug.security import check_password_hash
from werkzeug.security import generate_password_hash

from webforms import LoginForm
from webforms import PostForm
from webforms import UserForm

from web_constants import PAGE_404
from web_constants import PAGE_500
from web_constants import POST_EDIT_PAGE
from web_constants import POSTS_PAGE
from web_constants import POST_PAGE
from web_constants import INDEX_PAGE
from web_constants import DASHBOARD_PAGE
from web_constants import ADD_USER_PAGE
from web_constants import ADD_POST_PAGE
from web_constants import UPDATE_PAGE
from web_constants import LOGIN_PAGE

from web_constants import DELETE_USER_PATH
from web_constants import LOGOUT_PATH
from web_constants import DASHBOARD_PATH
from web_constants import DELETE_POST_PATH
from web_constants import LOGIN_PATH
from web_constants import UPDATE_USER_PATH
from web_constants import POSTS_PATH
from web_constants import EDIT_POST_PATH
from web_constants import POST_PATH
from web_constants import ADD_POST_PATH
from web_constants import ADD_USER_PATH
from web_constants import MAIN_PATH


class ApplicationConstructor:
    def __init__(self):
        self._app = Flask(__name__)

    @property
    def app(self):
        return self._app

    @staticmethod
    def define_login_managers(user_model, login_manager):
        @login_manager.user_loader
        def load_user(user_id):
            return user_model.query.get(int(user_id))

    def define_configurations(self, path_to_database, secret_key):
        self._app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{path_to_database}'
        self._app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        self._app.config['SECRET_KEY'] = secret_key

    def define_error_handlers(self):
        @self._app.errorhandler(404)
        def page_not_found(e):
            return render_template(PAGE_404), 404

        @self._app.errorhandler(500)
        def error_on_the_page(e):
            return render_template(PAGE_500), 500

    # TODO: Вынести дублирующийся код с datetime в отдельный метод
    def define_login_required_routers(self, database, user_model, user_post):
        @self._app.route(LOGOUT_PATH, methods=['GET', 'POST'])
        @login_required
        def logout():
            logout_user()
            flash("Successfully logged out")
            logger.info(f"User with id={current_user.id} successfully logged out")
            return redirect(url_for('login'))

        # TODO: Fix that old data is disappearing.
        @self._app.route(EDIT_POST_PATH, methods=['GET', 'POST'])
        @login_required
        def edit_post(identifier):
            post = user_post.query.get_or_404(identifier)
            post_form = PostForm()
            if current_user.id != post.user_id:
                flash("You aren't authorized to edit this post")
                posts = user_post.query.order_by(user_post.id)
                return render_template(POSTS_PAGE, posts=posts)

            if post_form.validate_on_submit():
                post.title = post_form.title.data
                post.slug = post_form.slug.data
                post.content = post_form.content.data

                database.session.add(post)
                database.session.commit()
                database.session.close()

                flash(f"Post is successfully updated")
                logger.info(f"Post with id={identifier} is successfully updated")
                return redirect(url_for('post', identifier=post.id))

            else:
                logger.info(f"Invalid data for post with id={identifier}")
                return render_template(POST_EDIT_PAGE, form=post_form)

        @self._app.route(ADD_POST_PATH, methods=['GET', 'POST'])
        @login_required
        def add_post():
            post_form = PostForm()
            if post_form.validate_on_submit():
                database.session.add(user_post(title=post_form.title.data,
                                               content=post_form.content.data,
                                               user_id=current_user.id,
                                               slug=post_form.slug.data))

                database.session.commit()
                database.session.close()

                post_form.title.data = ''
                post_form.content.data = ''
                post_form.slug.data = ''

                flash("Post is successfully submitted")
                logger.info("Post is successfully submitted")

            else:
                logger.error("Post is not submitted. Invalid data")

            return render_template(ADD_POST_PAGE, form=post_form)

        @self._app.route(DELETE_POST_PATH)
        @login_required
        def delete_post(identifier):
            post_to_delete = user_post.query.get_or_404(identifier)
            if current_user.id != post_to_delete.post.id:
                flash("You aren't authorized to delete this post")
                posts = user_post.query.order_by(user_post.id)
                return render_template(POSTS_PAGE, posts=posts)

            try:
                database.session.delete(post_to_delete)
                database.session.commit()

                flash(f"Post is successfully deleted")
                logger.info(f"Post with id={identifier} is successfully deleted")

                posts = user_post.query.order_by(user_post.id)

            except Exception as exc:
                flash(f"Post is not deleted")
                logger.error(f"Error. Post with id={identifier} is not deleted: {exc}")

                posts = user_post.query.order_by(user_post.id)
                database.session.rollback()

            finally:
                database.session.close()

            return render_template(POSTS_PAGE, posts=posts)

        @self._app.route(DASHBOARD_PATH, methods=['GET', 'POST'])
        @login_required
        def dashboard():
            form = UserForm()
            name_to_update = user_model.query.get_or_404(current_user.id)
            if request.method != "POST":
                return render_template(DASHBOARD_PAGE,
                                       form=form,
                                       name_to_update=name_to_update,
                                       id=current_user.id)

            name_to_update.email = request.form['email']
            name_to_update.username = request.form['username']
            database.session.commit()
            database.session.close()

            flash("User is successfully updated")
            logger.info("User is successfully updated")
            return render_template(DASHBOARD_PAGE,
                                   form=form,
                                   name_to_update=name_to_update)

        @self._app.route(UPDATE_USER_PATH, methods=['GET', 'POST'])
        @login_required
        def update(identifier):
            form = UserForm()
            user_to_update = user_model.query.get_or_404(identifier)
            if request.method != "POST":
                return render_template(UPDATE_PAGE,
                                       form=form,
                                       name_to_update=user_to_update,
                                       id=identifier)

            user_to_update.name = request.form['name']
            user_to_update.email = request.form['email']
            user_to_update.username = request.form['username']

            try:
                database.session.commit()
                flash(f"User is successfully updated")
                logger.info(f"User with id={identifier} is successfully updated")
                return render_template(UPDATE_PAGE,
                                       form=form,
                                       name_to_update=user_to_update, id=identifier)
            except Exception as exc:
                flash(f"User is not updated. Try again !")
                logger.error(f"User with id={identifier} is not updated: {exc}")
                database.session.rollback()
                return render_template(UPDATE_PAGE,
                                       form=form,
                                       name_to_update=user_to_update,
                                       id=identifier)
            finally:
                database.session.close()

        @self._app.route(DELETE_USER_PATH)
        @login_required
        def delete(identifier):
            if identifier != current_user.id:
                flash("You are not allowed to delete this user's profile")
                return redirect(url_for('dashboard'))

            user_to_delete = user_model.query.get_or_404(identifier)
            user_form = UserForm()

            try:
                database.session.delete(user_to_delete)
                database.session.commit()
                flash(f"User is successfully deleted")
                logger.info(f"User with id={identifier} is successfully deleted")

                return render_template(ADD_USER_PAGE,
                                       form=user_form,
                                       name=user_form.name.data,
                                       our_users=user_model.query.order_by(user_model.id))

            except Exception as exc:
                flash(f"User is not deleted. Try again !")
                logger.error(f'User with id={identifier} is not deleted: {exc}')

                database.session.rollback()
                return render_template(ADD_USER_PAGE,
                                       form=user_form,
                                       name=user_form.name.data,
                                       our_users=user_model.query.order_by(user_model.id))
            finally:
                database.session.close()

    def define_routers(self, database, user_model, user_post):
        @self._app.route(POSTS_PATH)
        def posts():
            return render_template(POSTS_PAGE,
                                   posts=user_post.query.order_by(user_post.id))

        @self._app.route(ADD_USER_PATH, methods=['GET', 'POST'])
        def add_user():
            user_form = UserForm()

            if not user_form.validate_on_submit():
                # TODO: Bug if username is aleady taken

                logger.error("User is not added")

                return render_template(ADD_USER_PAGE,
                                       form=user_form,
                                       our_users=user_model.query.order_by(user_model.id))

            user = user_model.query.filter_by(email=user_form.email.data).first()
            if user is None:
                hashed_pw = generate_password_hash(user_form.password_hash.data, "sha256")

                user = user_model(username=user_form.username.data,
                                  email=user_form.email.data,
                                  password_hash=hashed_pw)
                try:
                    database.session.add(user)
                    database.session.commit()
                    flash("User is successfully added")
                    logger.info("User is successfully added")

                except Exception as exc:
                    flash(f"Username is already taken")
                    logger.error(f"Username={user_form.username.data} is already taken: {exc}")
                    database.session.rollback()

                finally:
                    database.session.close()

            else:
                flash(f"User is already exists")
                logger.info(f"User with username={user_form.username.data} is already exists")

            user_form.username.data = ''
            user_form.email.data = ''
            user_form.password_hash.data = ''

            return render_template(ADD_USER_PAGE,
                                   form=user_form,
                                   our_users=user_model.query.order_by(user_model.id))

        @self._app.route(LOGIN_PATH, methods=['GET', 'POST'])
        def login():
            user_form = LoginForm()
            user_data = user_model.query.filter_by(username=user_form.username.data).first()

            if not user_form.validate_on_submit():
                logger.error('Data is not valid')
                return render_template(LOGIN_PAGE, form=user_form)

            elif not user_data:
                flash("Try again. User doesn't exist!")
                logger.error('User doesnt exist!')
                return render_template(LOGIN_PAGE, form=user_form)

            elif not check_password_hash(user_data.password_hash, user_form.password.data):
                flash("Try again. Password is incorrect")
                logger.error('Password is incorrect')
                return render_template(LOGIN_PAGE, form=user_form)

            else:
                login_user(user_data)
                flash("Logged in successfully")
                logger.info("Logged in successfully")
                return redirect(url_for('dashboard'))

        @self._app.route(POST_PATH)
        def post(identifier):
            return render_template(POST_PAGE,
                                   post=user_post.query.get_or_404(identifier))

        @self._app.route(MAIN_PATH)
        def index():
            return render_template(INDEX_PAGE, stuff="Demo version of Wiki")
